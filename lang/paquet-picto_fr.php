<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file
// 
// Module: paquet-animatecss


if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'picto_description' => 'Permet d\'utiliser FontAwesome sous SPIP',
	'picto_slogan' => 'Permet d\'utiliser FontAwesome sous SPIP',
);
?>