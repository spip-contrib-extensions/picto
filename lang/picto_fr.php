<?php
/**
 * Fichier de langue du plugin Picto avec FontAwesome
 *
 * @plugin     Picto avec FontAwesome
 * @copyright  © 2019
 * @author     Teddy Payet
 * @licence    GNU/GPL
 * @package    SPIP/Lang/
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'titre_page_picto' => 'Liste de FontAwesome',
);